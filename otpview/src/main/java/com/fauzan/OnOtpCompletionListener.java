package com.fauzan;

public interface OnOtpCompletionListener {
  void onOtpCompleted(String otp);

}
